<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;
use App\Products;

class ProductController extends Controller
{
    // penirimaan
    public function productReceipt()
    {
    	return view('report.product_receipt');
    }

    public function getDataProductReceipt(Request $request)
    {
    	$radio_status = $request->radio_status;
    	$kode_item = empty($request->kode_item) ? ' ' : $request->kode_item;

    	$date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::connection('kendaleco_live') 
                ->table('laporan_penerimaan');


        if ($request->radio_status == 'item') {
            $data = $data->where('Code', 'like', '%'.$kode_item.'%');
        }elseif ($request->radio_status == 'date') {
            $data = $data->where(function($query) use ($range) {
                        $query->whereBetween('TglMasuk', [$range['from'], $range['to']]);
                    });
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('Code', 'like', '%'.$filterby.'%')
                                ->orWhere('nama_barang', 'like', '%'.$filterby.'%');
                    });
        }


        return Datatables::of($data)
            ->editColumn('TglMasuk', function ($data) {
            	if (!empty($data->TglMasuk)) {
            		return Carbon::parse($data->TglMasuk)->format('d/m/Y');
            	}else{
            		return '-';
            	}
            })
            ->rawColumns(['TglMasuk'])
            ->make(true);
    }

    public function exportProductReceipt(Request $request)
    {
    	$radio_status = $request->radio_status;
    	$kode_item = $request->kode_item;

    	$orderby = $request->orderby;
    	$direction = $request->direction;

    	$date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::connection('kendaleco_live') 
                ->table('laporan_penerimaan');


        if ($request->radio_status == 'item') {
            $data = $data->where('Code', 'like', '%'.$kode_item.'%');
        }elseif ($request->radio_status == 'date') {
            $data = $data->where(function($query) use ($range) {
                        $query->whereBetween('TglMasuk', [$range['from'], $range['to']]);
                    });
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('Code', 'like', '%'.$filterby.'%')
                                ->orWhere('nama_barang', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('TglMasuk', 'asc');
        }

        $i = 1;

        $filename = 'report';

        $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
            $excel->sheet('report', function($sheet) use($data, $i, $range) {
                $sheet->appendRow(array(
                    '#', 'Date', 'Kode Item', 'Nama Barang', 'Qty'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i, $range)
                {
                    foreach ($rows as $row)
                    {
                        //
                        $sheet->appendRow(array(
                            $i++, Carbon::parse($row->TglMasuk)->format('d/m/Y'), $row->kode, $row->nama_barang, $row->qty
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }
    // end of penerimaan

    // pengeluaran
    public function productRelease()
    {
        return view('report.product_release');
    }

    public function getDataProductRelease(Request $request)
    {
        $radio_status = $request->radio_status;
        $kode_item = empty($request->kode_item) ? ' ' : $request->kode_item;

        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::connection('kendaleco_live') 
                ->table('laporan_penerimaan');


        if ($request->radio_status == 'item') {
            $data = $data->where('Code', 'like', '%'.$kode_item.'%');
        }elseif ($request->radio_status == 'date') {
            $data = $data->where(function($query) use ($range) {
                        $query->whereBetween('TglMasuk', [$range['from'], $range['to']]);
                    });
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('Code', 'like', '%'.$filterby.'%')
                                ->orWhere('nama_barang', 'like', '%'.$filterby.'%');
                    });
        }


        return Datatables::of($data)
            ->editColumn('TglMasuk', function ($data) {
                if (!empty($data->TglMasuk)) {
                    return Carbon::parse($data->TglMasuk)->format('d/m/Y');
                }else{
                    return '-';
                }
            })
            ->rawColumns(['TglMasuk'])
            ->make(true);
    }

    public function exportProductRelease(Request $request)
    {
        $radio_status = $request->radio_status;
        $kode_item = $request->kode_item;

        $orderby = $request->orderby;
        $direction = $request->direction;

        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::connection('kendaleco_live') 
                ->table('laporan_penerimaan');


        if ($request->radio_status == 'item') {
            $data = $data->where('Code', 'like', '%'.$kode_item.'%');
        }elseif ($request->radio_status == 'date') {
            $data = $data->where(function($query) use ($range) {
                        $query->whereBetween('TglMasuk', [$range['from'], $range['to']]);
                    });
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('Code', 'like', '%'.$filterby.'%')
                                ->orWhere('nama_barang', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('TglMasuk', 'asc');
        }

        $i = 1;

        $filename = 'report';

        $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
            $excel->sheet('report', function($sheet) use($data, $i, $range) {
                $sheet->appendRow(array(
                    '#', 'Date', 'Kode Item', 'Nama Barang', 'Qty'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i, $range)
                {
                    foreach ($rows as $row)
                    {
                        //
                        $sheet->appendRow(array(
                            $i++, Carbon::parse($row->TglMasuk)->format('d/m/Y'), $row->kode, $row->nama_barang, $row->qty
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }
}
