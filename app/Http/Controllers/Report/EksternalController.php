<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;
use App\Products;

class EksternalController extends Controller
{
    // penerimaan
    public function productReceipt()
    {
    	return view('report.product_receipt');
    }

    public function getDataProductReceipt(Request $request)
    {
    	$radio_status = $request->radio_status;
    	$kode_item = empty($request->kode_item) ? ' ' : $request->kode_item;

    	$date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::connection('kendaleco_live') 
                ->table('laporan_penerimaan');


        if ($request->radio_status == 'item') {
            $data = $data->where('Code', 'like', '%'.$kode_item.'%');
        }elseif ($request->radio_status == 'date') {
            $data = $data->where(function($query) use ($range) {
                        $query->whereBetween('TglMasuk', [$range['from'], $range['to']]);
                    });
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('Code', 'like', '%'.$filterby.'%')
                                ->orWhere('nama_barang', 'like', '%'.$filterby.'%');
                    });
        }


        return Datatables::of($data)
            ->editColumn('TglMasuk', function ($data) {
            	if (!empty($data->TglMasuk)) {
            		return Carbon::parse($data->TglMasuk)->format('d/m/Y');
            	}else{
            		return '-';
            	}
            })
            ->rawColumns(['TglMasuk'])
            ->make(true);
    }

    public function exportProductReceipt(Request $request)
    {
    	$radio_status = $request->radio_status;
    	$kode_item = $request->kode_item;

    	$orderby = $request->orderby;
    	$direction = $request->direction;

    	$date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::connection('kendaleco_live') 
                ->table('laporan_penerimaan');


        if ($request->radio_status == 'item') {
            $data = $data->where('Code', 'like', '%'.$kode_item.'%');
        }elseif ($request->radio_status == 'date') {
            $data = $data->where(function($query) use ($range) {
                        $query->whereBetween('TglMasuk', [$range['from'], $range['to']]);
                    });
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('Code', 'like', '%'.$filterby.'%')
                                ->orWhere('nama_barang', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('TglMasuk', 'asc');
        }

        $i = 1;

        $filename = 'report';

        $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
            $excel->sheet('report', function($sheet) use($data, $i, $range) {
                $sheet->appendRow(array(
                    '#', 'Date', 'Kode Item', 'Nama Barang', 'Qty'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i, $range)
                {
                    foreach ($rows as $row)
                    {
                        //
                        $sheet->appendRow(array(
                            $i++, Carbon::parse($row->TglMasuk)->format('d/m/Y'), $row->kode, $row->nama_barang, $row->qty
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }
    // end of penerimaan

    // pengeluaran
    public function productRelease()
    {
        return view('report.product_release');
    }

    public function getDataProductRelease(Request $request)
    {
        $radio_status = $request->radio_status;
        $kode_item = empty($request->kode_item) ? ' ' : $request->kode_item;

        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::connection('kendaleco_live') 
                ->table('laporan_pengiriman');


        if ($request->radio_status == 'item') {
            $data = $data->where('Code', 'like', '%'.$kode_item.'%');
        }elseif ($request->radio_status == 'date') {
            $data = $data->where(function($query) use ($range) {
                        $query->whereBetween('Tanggal_INV', [$range['from'], $range['to']]);
                    });
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('Code', 'like', '%'.$filterby.'%')
                                ->orWhere('nama_barang', 'like', '%'.$filterby.'%');
                    });
        }


        return Datatables::of($data)
            ->editColumn('Tanggal_INV', function ($data) {
                if (!empty($data->TglMasuk)) {
                    return Carbon::parse($data->TglMasuk)->format('d/m/Y');
                }else{
                    return '-';
                }
            })
            ->rawColumns(['Tanggal_INV'])
            ->make(true);
    }

    public function exportProductRelease(Request $request)
    {
        $radio_status = $request->radio_status;
        $kode_item = $request->kode_item;

        $orderby = $request->orderby;
        $direction = $request->direction;

        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::connection('kendaleco_live') 
                ->table('laporan_pengiriman');


        if ($request->radio_status == 'item') {
            $data = $data->where('Code', 'like', '%'.$kode_item.'%');
        }elseif ($request->radio_status == 'date') {
            $data = $data->where(function($query) use ($range) {
                        $query->whereBetween('Tanggal_INV', [$range['from'], $range['to']]);
                    });
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('Code', 'like', '%'.$filterby.'%')
                                ->orWhere('nama_barang', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('Tanggal_INV', 'asc');
        }

        $i = 1;

        $filename = 'report';

        $export = \Excel::create($filename, function($excel) use ($data, $i, $range) {
            $excel->sheet('report', function($sheet) use($data, $i, $range) {
                $sheet->appendRow(array(
                    '#', 'Date', 'Kode Item', 'Nama Barang', 'Qty'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i, $range)
                {
                    foreach ($rows as $row)
                    {
                        //
                        $sheet->appendRow(array(
                            $i++, Carbon::parse($row->TglMasuk)->format('d/m/Y'), $row->kode, $row->nama_barang, $row->qty
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }

    // end of pengeluaran

    // WIP
    public function productWip()
    {
        return view('report.product_wip');
    }

    public function getDataProductWip(Request $request)
    {
        $radio_status = $request->radio_status;
        $kode_item = empty($request->kode_item) ? ' ' : $request->kode_item;
        $filterby = $request->filterby;

        $date_range = $request->date_range != null ? Carbon::parse($request->date_range)->format('Y-m-d') : null;

        $data = DB::connection('kendaleco_live') 
                ->table('laporan_wip');


        if ($request->radio_status == 'item') {
            $data = $data->where('Code', 'like', '%'.$kode_item.'%');
        }elseif ($request->radio_status == 'date') {
            $data = $data->where('tanggal', $date_range);
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('Code', 'like', '%'.$filterby.'%')
                                ->orWhere('nama_barang', 'like', '%'.$filterby.'%');
                    });
        }


        return Datatables::of($data)
            ->editColumn('tanggal', function ($data) {
                if (!empty($data->tanggal)) {
                    return Carbon::parse($data->tanggal)->format('d/m/Y');
                }else{
                    return '-';
                }
            })
            ->rawColumns(['tanggal'])
            ->make(true);
    }

    public function exportProductWip(Request $request)
    {
        $radio_status = $request->radio_status;
        $kode_item = $request->kode_item;

        $orderby = $request->orderby;
        $direction = $request->direction;
        $filterby = $request->filterby;

        $date_range = $request->date_range != null ? Carbon::parse($request->date_range)->format('Y-m-d') : null;

        $data = DB::connection('kendaleco_live') 
                ->table('laporan_wip');


        if ($request->radio_status == 'item') {
            $data = $data->where('Code', 'like', '%'.$kode_item.'%');
            
            $f_name = 'item_like_'.$kode_item;
        }elseif ($request->radio_status == 'date') {
            $data = $data->where('tanggal', $date_range);

            $f_name = 'date_'.Carbon::parse($date_range)->format('d/m/Y');
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('Code', 'like', '%'.$filterby.'%')
                                ->orWhere('nama_barang', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('tanggal', 'asc');
        }

        $i = 1;

        $filename = 'report_wip_'.$f_name;

        $export = \Excel::create($filename, function($excel) use ($data, $i) {
            $excel->sheet('report', function($sheet) use($data, $i) {
                $sheet->appendRow(array(
                    'No', 'Tanggal', 'Kode Barang', 'Nama Barang', 'satuan', 'Jumlah', 'Keterangan'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i)
                {
                    foreach ($rows as $row)
                    {
                        //
                        $sheet->appendRow(array(
                            $i++, Carbon::parse($row->tanggal)->format('d/m/Y'), $row->Code, $row->nama_barang, $row->satuan, $row->Jumlah, $row->Keterangan
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }

    // end of WIP

    // Mutasi FG
    public function productMutasifg()
    {
        return view('report.product_mutasifg');
    }

    public function getDataProductMutasifg(Request $request)
    {
        $radio_status = $request->radio_status;
        $kode_item = empty($request->kode_item) ? '        ' : $request->kode_item;
        $filterby = $request->filterby;

        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::connection('kendaleco_live') 
                ->table('laporan_mutasi_fg');


        if ($request->radio_status == 'item') {
            $data = $data->where('code', 'like', '%'.$kode_item.'%');
        }elseif ($request->radio_status == 'date') {
            $data = $data->where(function($query) use ($range) {
                        $query->whereBetween('tanggal', [$range['from'], $range['to']]);
                    });
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('code', 'like', '%'.$filterby.'%')
                                ->orWhere('nama_barang', 'like', '%'.$filterby.'%');
                    });
        }

        $data_result = $data->orderby('tanggal', 'asc')->get()->toArray();

        $array = $data_result; // your array from above
        $result = array_reduce($array, function($carry, $item) { 
            if(!isset($carry[$item->code])) {
                $carry[$item->code] = $item;
            } else {
                $carry[$item->code]->masuk += $item->masuk;
                $carry[$item->code]->keluar += $item->keluar;
            }
            return $carry;
        });

        $result = $result !=null ? array_values($result) : array();

        foreach ($result as $results) {
            $results->saldo_akhir = $results->saldo_awal + $results->masuk - $results->keluar;
        }

        return Datatables::collection($result)
            ->editColumn('tanggal', function ($result) use($request) {
                if ($request->radio_status == 'date') {
                    return $request->date_range;
                }else{
                    if (!empty($result->tanggal)) {
                        return Carbon::parse($result->tanggal)->format('d/m/Y');
                    }else{
                        return '-';
                    }
                }
               
            })
            ->editColumn('balance', function ($result) {
                return $result->masuk - $result->keluar;
            })
            ->rawColumns(['tanggal'])
            ->make(true);
    }

    public function exportProductMutasifg(Request $request)
    {
        $radio_status = $request->radio_status;
        $kode_item = $request->kode_item;

        $orderby = $request->orderby;
        $filterby = $request->filterby;
        $direction = $request->direction;

        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = DB::connection('kendaleco_live') 
                ->table('laporan_mutasi_fg');


        if ($request->radio_status == 'item') {
            $data = $data->where('code', 'like', '%'.$kode_item.'%');
        }elseif ($request->radio_status == 'date') {
            $data = $data->where(function($query) use ($range) {
                        $query->whereBetween('tanggal', [$range['from'], $range['to']]);
                    });
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('code', 'like', '%'.$filterby.'%')
                                ->orWhere('nama_barang', 'like', '%'.$filterby.'%');
                    });
        }

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);
        }
        else {
            $data = $data->orderBy('tanggal', 'asc');
        }

        $i = 1;

        $data_result = $data->get()->toArray();

        $array = $data_result; // your array from above
        $result = array_reduce($array, function($carry, $item) { 
            if(!isset($carry[$item->code])) {
                $carry[$item->code] = $item;
            } else {
                $carry[$item->code]->masuk += $item->masuk;
                $carry[$item->code]->keluar += $item->keluar;
            }
            return $carry;
        });

        $result = $result !=null ? array_values($result) : array();

        foreach ($result as $results) {
            $results->saldo_akhir = $results->saldo_awal + $results->masuk - $results->keluar;
        }

        $filename = 'report';

        $export = \Excel::create($filename, function($excel) use ($result, $i, $range, $request) {
            $excel->sheet('report', function($sheet) use($result, $i, $range, $request) {
                $sheet->appendRow(array(
                    'No', 'Tanggal','Kode Barang', 'Nama Barang', 'Saldo Awal', 
                    'Masuk', 'keluar', 'Saldo Akhir', 'Penyesuaian', 'Stock Opname', 'Selisih', 'Keterangan'
                ));
                // $result->chunk(100, function($rows) use ($sheet, $i, $range, $request)
                // {
                    foreach ($result as $row)
                    {
                        //
                        if ($request->radio_status == 'date') {
                            $tanggal = $request->date_range;
                        }else{
                            if (!empty($row->tanggal)) {
                                $tanggal = Carbon::parse($row->tanggal)->format('d/m/Y');
                            }else{
                                $tanggal = '-';
                            }
                        }

                        $sheet->appendRow(array(
                            $i++, $tanggal, $row->code, $row->nama_barang, $row->saldo_awal, $row->masuk, $row->keluar, $row->saldo_akhir, $row->masuk-$row->keluar, $row->STOCK_OPNAME, $row->masuk-$row->keluar, $row->Keterangan 
                        ));
                    }
                // });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);
    }

    // end of Mutasi FG
}
