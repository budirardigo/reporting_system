<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    //
    protected $table = "products";
    protected $fillable = ['kode', 'name_barang', 'qty', 'created_at', 'updated_at'];
}
