<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($edit))
               <li><a href="{!! $edit !!}"><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            @if (isset($delete_product))
                <li><a href="{{ $delete_product }}" data-productid="{{ isset($productid) ? $productid : '' }}" class="ignore-click deleteProduct"><i class="icon-close2"></i> Delete</a></li>
            @endif

        </ul>
    </li>
</ul>
