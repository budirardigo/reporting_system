@extends('layouts.app', ['active' => 'productmurasifg'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><a href="{{ route('report.productMutasifg') }}"><i class="icon-dropbox position-left"></i> Laporan Mutasi Barang Jadi</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('report.ajaxGetDataProductMutasifg') }}" id="form_filter">
            <div class="form-group" id="filter_by_date">
                <label><b>Choose Date (month/day/year)</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="filter_by_item">
                <label><b>Choose Kode Item</b></label>
                <div class="input-group">
                    <input type="text" class="form-control" name="kode_item" id="kode_item" placeholder="Kode Item">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="date">Filter by Date</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="item">Filter by Kode Item</label>
            </div>
        </form>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-button-html5-basic" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Tanggal</th>
                        <th>Kode Barang</th>
                        <th>Nama Barang</th>
                        <th>Saldo Awal</th>
                        <th>Masuk</th>
                        <th>Keluar</th>
                        <th>Saldo Akhir</th>
                        <th>Penyesuaian</th>
                        <th>Stock Opname</th>
                        <th>Selisih</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('report.exportProductMutasifg') }}" id="export_report"></a>
@endsection

@include('report/_js_index')
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    //url
    var url = $('#form_filter').attr('action');
    var date = $('#date_range').val();
    var filter = null;

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();

    var table = $('#table-list').DataTable({
        // "paging": false,
        // "info": false,
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[1].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#export_report').attr('href')
                                            + '?date_range=' + date
                                            + '&orderby=' + column_name
                                            + '&direction=' + direction
                                            + '&kode_item=' + $('#kode_item').val()
                                            + '&radio_status=' + $('input[name=radio_status]:checked').val()
                                            + '&filterby=' + filter;
                }
            }
       ],
        ajax: {
            url: url,
            type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "radio_status": $('input[name=radio_status]:checked').val(),
                    "kode_item": $('#kode_item').val(),
                    "date_range": $('#date_range').val(),
                    "filterby": $('.dataTables_filter input').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(1).css('min-width', '150px');
            $('td', row).eq(2).css('min-width', '150px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'tanggal', name: 'tanggal'},
            {data: 'code', name: 'code'},
            {data: 'nama_barang', name: 'nama_barang'},
            {data: 'saldo_awal', name: 'saldo_awal'},
            {data: 'masuk', name: 'masuk'},
            {data: 'keluar', name: 'keluar'},
            {data: 'saldo_akhir', name: 'saldo_akhir'},
            {data: 'balance', name: 'balance', sortable: false, orderable: false, searchable: false},
           // {data: 'penyesuaian', name: 'penyesuaian'},
            {data: 'STOCK_OPNAME', name: 'STOCK_OPNAME'},
            {data: 'balance', name: 'balance', sortable: false, orderable: false, searchable: false},
            {data: 'Keterangan', name: 'Keterangan'}
        ],
    });

   table
    .on( 'preDraw', function () {
        Pace.start();
    } )
    .on( 'draw.dt', function () {
        Pace.stop();
        $('#table-list').unblock();
    } );

    //end of datatables

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        // //check location
        // if($('#date_range').val() == '') {
        //     alert('Please select date range first');
        //     return false;
        // }

        loading();

        table.draw();
    })


    $('#date_range').on('change', function(){
        date = $(this).val();
    })

     //choose filter
    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'item') {
            if($('#filter_by_item').hasClass('hidden')) {
                $('#filter_by_item').removeClass('hidden');
            }

            $('#filter_by_date').addClass('hidden');
        }
        else if (this.value == 'date') {
            if($('#filter_by_date').hasClass('hidden')) {
                $('#filter_by_date').removeClass('hidden');
            }

            $('#filter_by_item').addClass('hidden');
        }

        loading();

        table.draw();
    });

});



// loading
function loading() {
    $('#table-list').block({
        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: '10px 15px',
            color: '#fff',
            width: 'auto',
            '-webkit-border-radius': 2,
            '-moz-border-radius': 2,
            backgroundColor: '#333'
        }
    });
}

</script>
@endsection
