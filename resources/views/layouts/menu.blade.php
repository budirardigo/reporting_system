<!-- DASHBOARD -->
@permission(['menu-dashboard'])
<li class="{{ isset($active) && $active == 'dashboard' ? 'active' : '' }}">
    <a href="{{ route('dashboard') }}"> <span>DASHBOARD</span></a>
</li>
@endpermission
<!-- END OF DASHBOARD -->

<!-- MENU ADMIN -->
@permission(['menu-user-management'])
<li class="navigation-header"><span>User Management</span><i class="icon-menu" title="User Management"></i></li>
<li class="{{ isset($active) && $active == 'user' ? 'active' : '' }}">
    <a href="{{ route('user.index') }}"><i class="icon-users"></i><span>USER</span></a>
</li>
<li class="{{ isset($active) && $active == 'roles' ? 'active' : '' }}">
    <a href="{{ route('role.index') }}"><i class="glyphicon glyphicon-repeat"></i><span>ROLES</span></a>
</li>
@endpermission
<!-- END OF MENU ADMIN -->

<!-- MENU MASTER DATA -->
@permission(['menu-report', 'menu-internal', 'menu-eksternal'])
<li class="navigation-header"><span>Report</span><i class="icon-menu" title="User Management"></i></li>
@permission(['menu-eksternal'])
<li class="">
    <a href="#" class="has-ul legitRipple"><i class="icon-drawer-out"></i> <span>Eksternal</span></a>
    <ul class="hidden-ul" style="display: none;">
        <li class="{{ isset($active) && $active == 'productreceipt' ? 'active' : '' }}">
            <a href="{{ route('report.productReceipt') }}" class="legitRipple">Penerimaan</a>
        </li>
        <li class="{{ isset($active) && $active == 'productrelease' ? 'active' : '' }}">
            <a href="{{ route('report.productRelease') }}" class="legitRipple">Pengeluaran</a>
        </li>
        <li class="{{ isset($active) && $active == 'productwip' ? 'active' : '' }}">
            <a href="{{ route('report.productWip') }}" class="legitRipple">Barang Dalam Proses</a>
        </li>
        <li class="{{ isset($active) && $active == 'productMutasifg' ? 'active' : '' }}">
            <a href="{{ route('report.productMutasifg') }}" class="legitRipple">Mutasi Barang Jadi</a>
        </li>
        <li class="{{ isset($active) && $active == 'productMutasifg' ? 'active' : '' }}">
            <a href="{{ route('report.productMutasifg') }}" class="legitRipple">Mutasi Bahan Baku & Bahan Penolong</a>
        </li>
        <li class="{{ isset($active) && $active == 'productMutasifg' ? 'active' : '' }}">
            <a href="{{ route('report.productMutasifg') }}" class="legitRipple">Mutasi Scrap</a>
        </li>
        <li class="{{ isset($active) && $active == 'productMutasifg' ? 'active' : '' }}">
            <a href="{{ route('report.productMutasifg') }}" class="legitRipple">Mutasi Mesin dan Alat Kantor</a>
        </li>
    </ul>
</li>
@endpermission
@permission(['menu-internal'])
<li class="">
    <a href="#" class="has-ul legitRipple"><i class="icon-drawer-in"></i> <span>Internal</span></a>
    <ul class="hidden-ul" style="display: none;">
        <li>Traceability In to Out</li>
        <li>Traceability Out to In</li>
    </ul>
</li>
@endpermission
@endpermission
<!-- END OF MENU MASTER DATA -->