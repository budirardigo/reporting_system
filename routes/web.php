<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//lOGIN
Route::get('/', function () {
    if(Auth::check()) {

        return redirect()->route('dashboard');
    }
    return redirect('/login');
});

//LOGOUT DARURAT ONLY
Route::get('/logoutdarurat', function() {
    if(Auth::check()) {
        Auth::logout();
    }
    return redirect('/');
});

Route::get('/home', function() {
    if(Auth::check()) {
        return redirect('/');
    }
    return redirect('/login');
});

Auth::routes();

Route::middleware('auth')->group(function () {

	//DASHBOARD
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    //account setting
    Route::get('/user/account-setting/{id}', 'Admin\UserController@accountSetting')->name('user.accountSetting');
    Route::post('/user/account-setting/update-password/{id}', 'Admin\UserController@updatepassword')->name('user.updatepassword');

    //ROUTE ADMIN
    Route::prefix('admin')->group(function () {
        Route::middleware('role:admin')->group(function () {

            //user management
            Route::get('/user', 'Admin\UserController@index')->name('user.index');
            Route::get('/user/getdatatables', 'Admin\UserController@getDatatables')->name('user.getDatatables');
            Route::get('/user/add-user', 'Admin\UserController@create')->name('user.create');
            Route::get('/user/edit-user/{id}', 'Admin\UserController@edit')->name('user.edit');
            Route::post('/user/user-store', 'Admin\UserController@store')->name('user.store');
            Route::post('/user/update-user', 'Admin\UserController@update')->name('user.update');
            Route::get('/user/edit-user/{id}/reset-password', 'Admin\UserController@resetPassword')->name('user.resetPassword');
            Route::get('/user/delete-user/{id}', 'Admin\UserController@destroy')->name('user.delete');
            Route::get('/user-role', 'Admin\UserController@role')->name('user.role');

            //role
            Route::get('/role', 'Admin\RoleController@index')->name('role.index');
            Route::get('/role/getdatatables', 'Admin\RoleController@getDatatables')->name('role.getDatatables');
            Route::get('/role/add-role', 'Admin\RoleController@create')->name('role.create');
            Route::post('/role/role-store', 'Admin\RoleController@store')->name('role.store');
            Route::get('/role/edit-role/{id}', 'Admin\RoleController@edit')->name('role.edit');
            Route::post('/role/update-role', 'Admin\RoleController@update')->name('role.update');
            Route::get('/role/delete-role/{id}', 'Admin\RoleController@delete')->name('role.delete');
        });
    });

    //ROUTE REPORTING
    Route::prefix('report')->group(function () {
        //penirimaan
        Route::get('/product-receipt', 'Report\EksternalController@productReceipt')->name('report.productReceipt');
        Route::get('/product-receipt/export', 'Report\EksternalController@exportProductReceipt')->name('report.exportProductReceipt');
        Route::post('/get-report-product-receipt', 'Report\EksternalController@getDataProductReceipt')->name('report.ajaxGetDataProductReceipt');

        //pengeluaran
        Route::get('/product-release', 'Report\EksternalController@productRelease')->name('report.productRelease');
        Route::get('/product-release/export', 'Report\EksternalController@exportProductRelease')->name('report.exportProductRelease');
        Route::post('/get-report-product-release', 'Report\EksternalController@getDataProductRelease')->name('report.ajaxGetDataProductRelease');

        //wip
        Route::get('/product-wip', 'Report\EksternalController@productWip')->name('report.productWip');
        Route::get('/product-wip/export', 'Report\EksternalController@exportProductWip')->name('report.exportProductWip');
        Route::post('/get-report-product-wip', 'Report\EksternalController@getDataProductWip')->name('report.ajaxGetDataProductWip');

         //mutasi fg
        Route::get('/product-mutasifg', 'Report\EksternalController@productMutasifg')->name('report.productMutasifg');
        Route::get('/product-mutasifg/export', 'Report\EksternalController@exportProductMutasifg')->name('report.exportProductMutasifg');
        Route::post('/get-report-product-mutasifg', 'Report\EksternalController@getDataProductMutasifg')->name('report.ajaxGetDataProductMutasifg');
    });


});
