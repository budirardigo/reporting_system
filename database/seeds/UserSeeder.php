<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    public function run()
    {
        $roles  = Role::Select('id')->get();

        $user1 = new User();
        $user1->name = 'Admin';
        $user1->email = 'admin@mail.com';
        $user1->password =  bcrypt('admin');
        $user1->created_at =  Carbon::now();

        if($user1->save())
            $user1->attachRoles($roles);
    }
}
