<?php use Illuminate\Database\Seeder;

use App\Role;
use App\Permission;

class RoleSeeder extends Seeder
{
    public function run()
    {
        $permissions = Permission::select('id')->get();
        $role = new Role();
        $role->name = 'admin';
        $role->display_name = 'admin';
        $role->description  = 'super admin';


        if($role->save()){
            $role->attachPermissions($permissions);
        }
    }
}
