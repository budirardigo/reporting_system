<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'menu-report',
                'display_name' => 'menu report',
                'description' => 'menu report'
            ],
            [
                'name' => 'menu-user-management',
                'display_name' => 'menu user management',
                'description' => 'menu user management'
            ]
        ];

        foreach ($permissions as $key => $permission) {
            Permission::create([
                'name' => $permission['name'],
                'display_name' => $permission['display_name'],
                'description' => $permission['description']
            ]);
        }
    }
}
