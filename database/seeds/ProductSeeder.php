<?php

use Illuminate\Database\Seeder;
use App\Products;
use Carbon\Carbon;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $products = [
            [
                'kode' => '001',
                'nama_barang' => 'Roti Mari',
                'qty' => 100,
                'created_at' => Carbon::now()
            ],
            [
                'kode' => '002',
                'nama_barang' => 'dji sam soe',
                'qty' => 105,
                'created_at' => Carbon::now()
            ],
            [
                'kode' => '003',
                'nama_barang' => 'sampoerna',
                'qty' => 200,
                'created_at' => Carbon::now()
            ],
            [
                'kode' => '004',
                'nama_barang' => 'MLD',
                'qty' => 210,
                'created_at' => Carbon::now()
            ],
            [
                'kode' => '005',
                'nama_barang' => 'kretek',
                'qty' => 50,
                'created_at' => Carbon::now()
            ],
            [
                'kode' => '006',
                'nama_barang' => 'pro mild',
                'qty' => 65,
                'created_at' => Carbon::now()
            ],
            [
                'kode' => '007',
                'nama_barang' => 'filter',
                'qty' => 90,
                'created_at' => Carbon::now()
            ],
            [
                'kode' => '008',
                'nama_barang' => 'djarum 76',
                'qty' => 78,
                'created_at' => Carbon::now()
            ]
        ];

        foreach ($products as $key => $product) {
            Products::create([
                'kode' => $product['kode'],
                'nama_barang' => $product['nama_barang'],
                'qty' => $product['qty'],
                'created_at' => $product['created_at']
            ]);
        }
    }
}
